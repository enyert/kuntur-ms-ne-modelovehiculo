package com.pacifico.kuntur.model.dao;

import lombok.Data;

@Data
public class ModeloVehiculoDao {

    private String tipoRie;
    private String tipoVeh;
    private String claseVeh;

    private String codModelo;
    private String descripcionModelo;
    private String codigoModeloApeseg;
    private String descModeloStd;
    private String descCategoriaMtc;

    private String numPue;
    private String descTransmision;

    private String codigoRamo;
}