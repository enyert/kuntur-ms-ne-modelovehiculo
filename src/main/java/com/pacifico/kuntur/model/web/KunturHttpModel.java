package com.pacifico.kuntur.model.web;

import com.pacifico.kuntur.exception.HttpModelValidationException;

public abstract class KunturHttpModel<T> {
    private Boolean valid = false;

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public Boolean isValid() {
        return this.valid;
    }

    public abstract Boolean validate() throws HttpModelValidationException;

    public abstract String buildLogRepresentation(T model);

    public abstract String buildViolationsRepresentation(T model);
}
