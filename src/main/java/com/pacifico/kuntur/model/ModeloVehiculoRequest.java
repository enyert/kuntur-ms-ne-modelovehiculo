package com.pacifico.kuntur.model;

import com.pacifico.kuntur.exception.HttpModelValidationException;
import com.pacifico.kuntur.model.web.KunturHttpModel;
import com.pacifico.kuntur.validation.integrity.KunturGenericHttpModelValidator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ModeloVehiculoRequest extends KunturHttpModel<ModeloVehiculoRequest> {

    @NotNull
    @NotEmpty
    @Pattern(regexp = "[a-zA-Z0-9 ]")
    private String codigoMarca;

    @NotNull
    @NotEmpty
    @Pattern(regexp = "[a-zA-Z0-9 ]")
    private String codigoRamo;

    @Override
    public Boolean validate() throws HttpModelValidationException {
        KunturGenericHttpModelValidator<ModeloVehiculoRequest> validator =
                new KunturGenericHttpModelValidator<>(this);
        return validator.recollectViolations(this).isEmpty();
    }

    @Override
    public String buildLogRepresentation(ModeloVehiculoRequest model) {
        return null;
    }

    @Override
    public String buildViolationsRepresentation(ModeloVehiculoRequest model) {
        return null;
    }
}
