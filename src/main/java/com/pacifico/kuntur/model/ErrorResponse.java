package com.pacifico.kuntur.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import com.pacifico.kuntur.exception.HttpModelValidationException;
import com.pacifico.kuntur.model.web.KunturHttpModel;
import com.pacifico.kuntur.validation.integrity.GenericHttpModelValidator;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ErrorResponse extends KunturHttpModel<ErrorResponse>{

	@NotNull(message="El campo estatus HTTP no puede ser nulo")
	private Integer estatusHTTP;
	
	@NotEmpty(message="El campo codigo no puede ser vacio")
	@NotNull(message="El campo codigo no puede ser nulo")
	private String codigo;
	
	@NotEmpty(message="El campo mensaje no puede ser vacio")
	@NotNull(message="El campo mensaje no puede ser nulo")
	private String mensajeExterno;
	
	private String mensajeInterno;
	
	/**
     * This method evaluates the consistency of a given request
     * @return boolean que indica si el bean es valido
     * @throws HttpModelValidationException 
     */
	@Override
	public Boolean validate() throws HttpModelValidationException {
		GenericHttpModelValidator<ErrorResponse> requestValidator =
                new GenericHttpModelValidator<>(this);
		String violaciones = requestValidator.exposeViolations();
		if(!requestValidator.isValidBean()) {
			throw new HttpModelValidationException("002",violaciones);
		}
		return true;
	}

	/**
     * This method construct a log representation for this object
     * @return
     */
	@Override
	public String buildLogRepresentation(ErrorResponse model) {
		return String.format("Receiving: %s", this.toString());
	}

	/**
     * This method is used to build the violations's representations
     * @return
     */
	@Override
	public String buildViolationsRepresentation(ErrorResponse model) {
		GenericHttpModelValidator<ErrorResponse> requestValidator =
                new GenericHttpModelValidator<>(this);
		return requestValidator.exposeViolations();
	}

}
