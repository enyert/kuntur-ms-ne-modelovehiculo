package com.pacifico.kuntur.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ModeloVehiculoResponse {

    private String tipoRie;
    private String tipoVeh;
    private String claseVeh;

    private String codModelo;
    private String descripcionModelo;
    private String codigoModeloApeseg;
    private String descModeloStd;
    private String descCategoriaMtc;

    private String numPue;
    private String descTransmision;

    private String codigoRamo;
}
