package com.pacifico.kuntur.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Proyecto Arquitectura Digital Pacifico
 * MVP Microservicios
 * Para: Pacifico Seguros
 * Everis Peru
 * Microservicio: spring-boot-reactive-restful-nosql-mongodb
 *
 * @author Anthony Pinero
 * @version 1.0
 * @email: apineror@everis.com
 * @since 30/06/18
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any()).apis(Predicates.not(RequestHandlerSelectors.basePackage("org.springframework.boot")))
                .build();
    }
}