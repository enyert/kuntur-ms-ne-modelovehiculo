package com.pacifico.kuntur.business;

import java.util.List;

import com.pacifico.kuntur.model.ModeloVehiculoRequest;
import com.pacifico.kuntur.model.ModeloVehiculoResponse;

public interface ModeloVehiculoService {
    List<ModeloVehiculoResponse> consultarModelo(ModeloVehiculoRequest request);
}
