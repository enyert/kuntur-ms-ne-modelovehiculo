package com.pacifico.kuntur.business.impl;

import com.pacifico.kuntur.business.ModeloVehiculoService;
import com.pacifico.kuntur.model.KunturServiceContract;
import com.pacifico.kuntur.model.ModeloVehiculoRequest;
import com.pacifico.kuntur.model.ModeloVehiculoResponse;
import com.pacifico.kuntur.model.dao.ModeloVehiculoDao;
import com.pacifico.kuntur.repository.ModeloVehiculoRepository;
import com.pacifico.kuntur.validation.business.KunturBusinessValidation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ModeloVehiculoServiceImpl implements ModeloVehiculoService, KunturServiceContract<ModeloVehiculoRequest, ModeloVehiculoResponse> {

    @Autowired
    private ModeloVehiculoRepository modeloVehiculoRepository;

    private Function<ModeloVehiculoDao, ModeloVehiculoResponse> transformDaoToResponse = t -> {
        ModeloVehiculoResponse response = new ModeloVehiculoResponse();
        response.setClaseVeh(t.getClaseVeh());
        response.setCodigoModeloApeseg(t.getCodigoModeloApeseg());
        response.setCodigoRamo(t.getCodigoRamo());
        response.setCodModelo(t.getCodModelo());
        response.setDescCategoriaMtc(t.getDescCategoriaMtc());
        response.setClaseVeh(t.getClaseVeh());
        response.setDescModeloStd(t.getDescModeloStd());
        response.setDescripcionModelo(t.getDescripcionModelo());
        response.setDescTransmision(t.getDescTransmision());
        return response;
    };

    @Override
    public List<ModeloVehiculoResponse> consultarModelo(ModeloVehiculoRequest request) {
        request = prepareRequest(request);

        List<ModeloVehiculoDao> partialResult = modeloVehiculoRepository.obtenerModelosVehiculo(request.getCodigoMarca(),
                request.getCodigoRamo());
        return partialResult.stream().map(t -> transformDaoToResponse.apply(t)).collect(Collectors.toList());
    }

    @Override
    public ModeloVehiculoRequest prepareRequest(ModeloVehiculoRequest request) {
        return request; // Identity because we have no modifications over this object
    }

    @Override
    public Boolean applyPreValidation(List<KunturBusinessValidation<ModeloVehiculoRequest>> kunturBusinessValidations) {
        return true; //TODO Implement business pre-validations. In case we need it
    }

    @Override
    public Boolean applyPostValidation(List<KunturBusinessValidation<ModeloVehiculoResponse>> kunturBusinessValidations) {
        return true; //TODO Implement business post-validations. In case we need it
    }


}