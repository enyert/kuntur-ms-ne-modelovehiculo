package com.pacifico.kuntur.repository.impl;

import com.pacifico.kuntur.model.dao.ModeloVehiculoDao;
import com.pacifico.kuntur.repository.ModeloVehiculoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class ModeloVehiculoRepositoryImpl implements ModeloVehiculoRepository {

    @Autowired
    EntityManager entityManager;

    private final String PROCEDURE_NAME = "PO_MODELO_VEH.Consultar";

    /**
     * Obtiene los modelos dados los parametros codigoMarca y codigoRamo
     * @param codigoMarca Representa el codigo de marca asociado a los modelos que se quieren consultar
     * @param codigoRamo Representa el ramo al que pertenece la marca asociada
     * @return
     */
    @Override
    public List<ModeloVehiculoDao> obtenerModelosVehiculo(String codigoMarca, String codigoRamo) {
        List<Object> dbObjects = consultaListaObjetosBD(codigoMarca,codigoRamo);
        List<ModeloVehiculoDao> modelos = mapperObjToModeloVehDao(dbObjects);
        return modelos;
    }

    /**
     * Metodo que consulta el store procedure que soporta a modelo
     * @param codigoMarca Representa el codigo de marca asociado a los modelos que se quieren consultar
     * @param codigoRamo Representa el ramo al que pertenece la marca asociada
     * @return
     */
    @SuppressWarnings("unchecked")
	@Override
    public List<Object> consultaListaObjetosBD(String codigoMarca, String codigoRamo) {

        StoredProcedureQuery proc = entityManager.createStoredProcedureQuery(PROCEDURE_NAME);
        proc.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter(4, String.class, ParameterMode.IN);
        proc.registerStoredProcedureParameter(5, String.class, ParameterMode.REF_CURSOR);
        proc.setParameter(1, null);
        proc.setParameter(2, codigoMarca);
        proc.setParameter(3, codigoRamo);
        proc.setParameter(4, null);
        proc.execute();

        return proc.getResultList();
    }

    private List<ModeloVehiculoDao> mapperObjToModeloVehDao(List<Object> objects) {

        List<ModeloVehiculoDao> respuesta = new ArrayList<>();
        Iterator<Object> itr = objects.iterator();

        while(itr.hasNext()){

            Object[] obj = (Object[]) itr.next();
            ModeloVehiculoDao modeloVehDao = new ModeloVehiculoDao();

            modeloVehDao.setCodModelo(String.valueOf(obj[0]));
            modeloVehDao.setCodigoModeloApeseg(String.valueOf(obj[1]));
            modeloVehDao.setDescripcionModelo(String.valueOf(obj[2]));
            modeloVehDao.setDescTransmision(String.valueOf(obj[3]));
            modeloVehDao.setTipoRie(String.valueOf(obj[4]));
            modeloVehDao.setNumPue(String.valueOf(obj[5]));
            modeloVehDao.setTipoVeh(String.valueOf(obj[6]));
            modeloVehDao.setClaseVeh(String.valueOf(obj[7]));
            modeloVehDao.setCodigoRamo(String.valueOf(obj[8]));
            modeloVehDao.setDescModeloStd(String.valueOf(obj[9]));
            modeloVehDao.setDescCategoriaMtc(String.valueOf(obj[10]));

            respuesta.add(modeloVehDao);
        }

        return respuesta;
    }
}
