package com.pacifico.kuntur.repository;

import java.util.List;

import com.pacifico.kuntur.model.dao.ModeloVehiculoDao;

public interface ModeloVehiculoRepository {

    /**
     * Obtiene los modelos dados los parametros codigoMarca y codigoRamo
     * @param codigoMarca Representa el codigo de marca asociado a los modelos que se quieren consultar
     * @param codigoRamo Representa el ramo al que pertenece la marca asociada
     * @return
     */
    public List<ModeloVehiculoDao> obtenerModelosVehiculo(String codigoMarca, String codigoRamo);

    /**
     * Metodo que consulta el store procedure que soporta a modelo
     * @param codigoMarca Representa el codigo de marca asociado a los modelos que se quieren consultar
     * @param codigoRamo Representa el ramo al que pertenece la marca asociada
     * @return
     */
    public List<Object> consultaListaObjetosBD(String codigoMarca, String codigoRamo);



}
