package com.pacifico.kuntur.exception;

public class HttpModelValidationException extends Exception {

	private static final long serialVersionUID = -975053519615702791L;
	
	private String code;

	public HttpModelValidationException() {
		super();
		this.code = "";
    }

    public HttpModelValidationException(String message) {
    	super(message);
		this.code = "";
    }

    public HttpModelValidationException(String message, Throwable cause) {
    	super(message, cause);
		this.code = "";
    }
    
    public HttpModelValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.code = "";
	}
    
    public HttpModelValidationException(Throwable cause) {
		super(cause);
		this.code = "";
	}
	
	public HttpModelValidationException(Throwable cause, String code) {
		super(cause);
		this.code = code;
	}
	
	public HttpModelValidationException(String code, String message) {
		super(message);
		this.code = code;
	}

	/**
	 * @return El codigo de la excepcion
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param Asigna el codigo de la excepcion
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
}
