package com.pacifico.kuntur.exception;

public class BusinessException extends Exception {

	private static final long serialVersionUID = -9124304114560031911L;
	
	private String code;

	public BusinessException() {
		super();
		this.code = "";
	}

	public BusinessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.code = "";
	}

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
		this.code = "";
	}

	public BusinessException(String message) {
		super(message);
		this.code = "";
	}

	public BusinessException(Throwable cause) {
		super(cause);
		this.code = "";
	}
	
	public BusinessException(Throwable cause, String code) {
		super(cause);
		this.code = code;
	}
	
	public BusinessException(String code, String message) {
		super(message);
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
