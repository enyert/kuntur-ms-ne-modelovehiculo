package com.pacifico.kuntur.expose.advice;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.pacifico.kuntur.exception.BusinessException;
import com.pacifico.kuntur.exception.HttpModelValidationException;
import com.pacifico.kuntur.model.ErrorResponse;

@ControllerAdvice
public class ExposeControllerAdvice extends ResponseEntityExceptionHandler {
	
	private ResponseEntity<ErrorResponse> generarErrorResponse(final Exception e, final HttpStatus estatusHTTP, String codigoError, String mensajeError) {
        final String mensajeInterno = Optional.of(e.getMessage()).orElse(e.getClass().getSimpleName());
        ErrorResponse errorResponse = new ErrorResponse(estatusHTTP.value(), codigoError, mensajeError, mensajeInterno);
        return new ResponseEntity<>(errorResponse, estatusHTTP);
    }

	@ExceptionHandler(value = { BusinessException.class })
	public ResponseEntity<ErrorResponse> handleBusinessException(BusinessException e, WebRequest webRequest){
		return generarErrorResponse(e,HttpStatus.CONFLICT,e.getCode(),e.getMessage());
	}

	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<ErrorResponse> handleAllException(RuntimeException e, WebRequest webRequest){
		return generarErrorResponse(e,HttpStatus.INTERNAL_SERVER_ERROR,"001","Error general en el servicio");
	}
	
	@ExceptionHandler(value = { HttpModelValidationException.class })
	public ResponseEntity<ErrorResponse> handleBeanValidationException(HttpModelValidationException e, WebRequest webRequest){
		return generarErrorResponse(e,HttpStatus.BAD_REQUEST,e.getCode(),e.getMessage());
	}

}
