package com.pacifico.kuntur.expose;

import com.pacifico.kuntur.business.ModeloVehiculoService;
import com.pacifico.kuntur.exception.HttpModelValidationException;
import com.pacifico.kuntur.model.ModeloVehiculoRequest;
import com.pacifico.kuntur.model.ModeloVehiculoResponse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
public class ModeloVehiculoController {

    @Autowired
    private ModeloVehiculoService modeloVehiculoService;

	@SuppressWarnings("unused")
	private Mono<List<ModeloVehiculoResponse>> getCliente(@RequestParam("codigoMarca") String codMarca,
                                                          @RequestParam("codigoRamo") String codRamo) throws HttpModelValidationException {
        log.info("El servicio getCliente esta siendo usado");

        ModeloVehiculoRequest request = new ModeloVehiculoRequest(codMarca, codRamo);

        String requestRepresentation = request.buildLogRepresentation(request);

        log.debug(requestRepresentation);

        List<ModeloVehiculoResponse> modeloVehiculoResponse = new ArrayList<>();

        if(request.validate()) {
            modeloVehiculoResponse = modeloVehiculoService.consultarModelo(request);
        } else {
            log.error("Ha ocurrido un error en el request " + requestRepresentation);
        }

        return Mono.just(modeloVehiculoResponse);
    }

}
