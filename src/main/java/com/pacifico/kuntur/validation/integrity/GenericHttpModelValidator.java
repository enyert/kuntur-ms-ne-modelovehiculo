package com.pacifico.kuntur.validation.integrity;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import com.google.gson.Gson;

/**
 * Clase generica para realizar la validacion de un bean tipo de tipo B
 * @param <T> Tipo base del bean a validar
 */
public class GenericHttpModelValidator<T> {
	private T request;
    private Boolean isValid;

    /**
     * Constructor para la clase GenericHttpModelValidator
     * @param request T class that represents the request built from the controller's parameters
     */
    public GenericHttpModelValidator(T request) {
        this.request = request;
        this.isValid = false;
    }

    /**
     * Este metodo recolecta todas las violaciones desde un bean invalido
     * @return
     */
    private Set<ConstraintViolation<T>> recollectValidations() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        return validator.validate(this.request);
    }

    /**
     * Este metodo expone todas las violaciones(En caso de que existan) del bean en cuestion
     * @return
     */
    public String exposeViolations() {
        Set<ConstraintViolation<T>> violations = recollectValidations();
        String result = "";
        this.isValid = violations.isEmpty();
        if(!this.isValid) {
            result = formatViolations(violations);
        }
        return result;
    }

    /**
     * Metodo para verificar la validez de un bean
     * @return
     */
    public Boolean isValidBean() {
    	return this.isValid;
    }

    /**
     * Metodo para formatear el conjunto de violaciones que se detecten sobre un bean especifico
     * @param input
     * @return
     */
    private String formatViolations(Set<ConstraintViolation<T>> input) {
        List<String> result = input.stream().map(t -> t.getMessage()).collect(Collectors.toList());
        return new Gson().toJson(result);
    }
}
