package com.pacifico.kuntur;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.pacifico.kuntur"})
public class PacificoKunturMsNegocioModeloApplication {

	public static void main(String[] args) {
		SpringApplication.run(PacificoKunturMsNegocioModeloApplication.class, args);
	}
}
