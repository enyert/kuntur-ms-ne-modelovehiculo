# Arquitectura Digital - Pacífico Seguros

Implementación de una arquitectura digital en Pacífico Seguros

## Comienzo

Estas instrucciones le permitirán obtener una copia del proyecto en funcionamiento en su máquina local para fines de desarrollo y prueba. Consulte la implementación para obtener notas sobre cómo implementar el proyecto.

### Prerrequisitos

Por construir...

### Instalación

Por construir...

## Ejecución de las pruebas

Por construir...

### Análisis de prueba end to end

Por construir...

### Pruebas de estilo de codificación

Por construir...

## Despliegue

Por construir...

## Contruido con

* [SpringBoot](https://spring.io/projects/spring-boot) - El framework utilizado para crear microservicios
* [Gradle](https://gradle.org/) - Dependency Management

## Contribución

Por construir...

## Versionamiento

Por construir...

## Autores

[everis Perú](https://www.everis.com/peru/es/home-peru)

Por construir la lista de colaboradores...

## Licencias

Por construir...

## Agradecimientos

Por construir... 
